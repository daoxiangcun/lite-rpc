package com.yhl.rpc.server;

import org.apache.log4j.BasicConfigurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        RpcNettyServer rpcNettyServer = (RpcNettyServer) appContext.getBean("rpcNettyServer");

        long count = 0;
        BasicConfigurator.configure();
        while (true) {
            Thread.sleep(1000);
            count++;
            System.out.println("==== " + count + " ======");
        }
    }
}
